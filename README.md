# UARDECS_MEGA
UARDECS is a library for implementing a protocol based on "UECS" which is a communication standard for Japanese horticulture. This library has greatly increased the items that can be set in the advanced version of UARDECS, but the memory consumption has increased and the operation can not be guaranteed without Arduino MEGA. To run this library, you need to install a shield with Ethernet Shield 2 or W5500. Please install Ethernet2 library together when using this library.

UARDECSは日本の施設園芸用の通信規格である"UECS"準拠のプロトコルをArduinoに実装するためのライブラリです。このライブラリはUARDECSの高機能バージョンで設定できる項目が大幅に増えていますが、メモリの消費量が増えているためArduino MEGAでなければ動作は保証できません。このライブラリを実行するにはEthernet Shield2 あるいはW5500を搭載したシールドを装着する必要があります。このライブラリを使うときは一緒にEthernet2ライブラリをインストールして下さい。

詳細なマニュアル(日本語版)は以下で提供されます。
https://uecs.org/arduino/uardecs.html

# Modified by HOLLY branch description

## Networkの変更点

### IPアドレスの決定はDHCPに委ねる

 基本的にDHCPを使うことで他のデバイスや装置との干渉も少なく設定も容易な事から規定値としてはDHCPによるアドレス取得とする。  
 固定IPモードも用意する。固定IPモードにするためのモードスイッチは外付きにするか、コマンドでEEPROM内に持たせるか。また、どのように固定IPを設定するかなど、全て、現在検討中。

### MACアドレスはEEPROMに事前に書き込んでおく

 現在、コード内にプログラムとして記述されているMACアドレスだが、この方式だと装置を量産する時にプログラムも同じ台数分、MACアドレスを変えるためだけに必要となる。githubなどで版管理していたらとんでもないことになります。

 本来はハードウェア固有唯一のアドレスであるのでハードウェアに書き込んでおくことが適当と判断しての結果。

### 状況表示のLCDを用いて表示する。

 DHCPなどでアドレスを取得するとなると、現在設定されているIPアドレスは何なのか？がわからないことになる。そのため、設定状況や内容を表示するための表示器が必要不可欠となる。
 LCDはそのために欠かせない。  
 LCDはI2Cインタフェースのもので、I2CアドレスはEEPROMに書き込んでおく。

## EEPROM MAP


| Address | Name | Type | Comment |
|:-------:|:-----|:-----|:--------|
| 2476    | EEPROM_PROGRAMDATETIME | --- | |
| 2500    | EEPROM_CCMTOP | --- | |
| 3399    | EEPROM_CCMEND | --- | |
| 3400    | EEPROM_DATATOP | PTR | |
| 3400    | EEPROM_IP | byte &times; 4 | IPアドレス |
| 3404    | EEPROM_SUBNET | byte &times; 4 | サブネットマスク |
| 3408    | EEPROM_GATEWAY | byte &times; 4 | デフォルトゲートウェイ |
| 3412    | EEPROM_DNS | byte &times; 4 | DNSサーバ |
| 3416    | EEPROM_ROOM | byte | CCMのROOM |
| 3417    | EEPROM_REGION | byte | CCMのREGION |
| 3418    | EEPROM_ORDER_L | byte | CCMのORDER |
| 3419    | EEPROM_ORDER_H | byte | CCMのORDER_H |
| 3425    | EEPROM_NODENAME | byte | ノード名 |
| 3450    | EEPROM_WEBDATA | byte | CCMのORDER_H |
| 4095    | EEPROM_DATAEND | PTR | EEPROMの終わり |






